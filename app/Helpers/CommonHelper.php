<?php

namespace App\Helpers;

use Logger;
use Illuminate\Http\Request;

class CommonHelper{

    public  $logName    = "CommonHelper";
    protected $logger;

    public function __construct($newLogName = ""){
        if($newLogName != ""){
            $this->logName = $newLogName;
        }
		if (isset($this->logName)) {  
			$this->logger = new Logger($this->logName);
		}
	}

    public function curl($url, $body = null, $header = [], $method = "GET")
    {
        $curl = curl_init();

        if (is_array($body)) {
            $body = http_build_query($body);
        }

        $this->logger->info("CURL REQUEST \tHeader:: " . json_encode($header) . "\t Method:: " . $method . "\t" . $body . "\tURL::" . $url);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $this->logger->info("CURL RESPONSE :\t" . $response);
            return json_decode($response, true);
        }
    }
    
}

?>