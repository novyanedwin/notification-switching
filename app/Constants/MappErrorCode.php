<?php
namespace App\Constants;

class MappErrorCode
{
    const VENDOR_NOT_AVAILABLE = [
        'status' => "error",
        'code' => 400,
        'message' => "Vendor Not Available!",
    ];

	const VENDOR_NOT_IMPLEMENTED = [
        'status' => "error",
        'code' => 500,
        'message' => "Vendor Not Implemented!",
    ];

}
