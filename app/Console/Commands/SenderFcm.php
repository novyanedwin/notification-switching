<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Helpers\CommonHelper;
use App\Models\Queueing;
use App\Models\QueueingReport;
use Logger;

if (!defined("SESSIONID")) define("SESSIONID", uniqid());

class SenderFcm extends Command
{
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "worker:fcm {thread_number=null} {priority=null} {limit=100}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "worker:fcm {thread_number} {priority} {limit=100}";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    protected $code = "fcm";
    protected $batchName = "";
    protected $logger;
    
    public function handle()
    {
        $logPrefix = __FUNCTION__."\t\t";
        $this->logger = new Logger("Job_Sender_".strtoupper($this->code));

        $priority   = $this->argument("priority");
        $tNumber    = $this->argument("thread_number");
        $limit      = $this->argument("limit");
        $code       = $this->code;

        $this->batchName = "sender_".$code."_".$tNumber."_".$priority;

        $ch =  new CommonHelper;

        if($ch->checkRunning($this->batchName)){
            $this->logger->error($logPrefix."Job Is Still Running : ".$this->batchName);
            exit("Still Running");
        }

        if(!$ch->doQueueing($code)){
            $this->logger->info("Nothing Executed : ".$this->batchName);
            exit("Nothing Executed");
        }
        
        if((int) $limit > 100){
            $this->logger->error($logPrefix."Forcing Limit to 100, Limit Requested : ".$limit);
            $limit = 100;
        }
        
        $ch->batchRunning($this->batchName, "true"); //lock process
        $data = Queueing::where('code', $code)
                ->where('status', '0')
                ->where(function($query) use($tNumber, $priority){
                    if($tNumber != "null"){
                        $query->where('id', 'like', '%'.$tNumber);
                    }
                    if($priority != "null"){
                        $query->where('priority', $priority);
                    }
                })
                ->limit($limit)
                ->orderBy(($priority != "null" ? 'id' : 'priority'), ($priority != "null" ? 'asc' : 'desc'))
                ->get()
                ->toArray();
        
        if(count($data) < 1){
            $ch->batchRunning($this->batchName, "false");
            $ch->setQueueing($code, "false");
            exit("Empty Data");
        }
        $idList = array_column($data, "id");
        $item = count($idList);
        
        $updateId = Queueing::whereIn('id', $idList)
                    ->update(['status' => "1"]);
        
        if($item != $updateId){
            $this->logger->error($logPrefix."Unmatch Update : ".$updateId." != ".$item);
        }
        $ch->batchRunning($this->batchName, "false");
        //open lock

        $reqLeft = Queueing::selectRaw("count(id) as total")
                    ->where('code', $code)
                    ->where('status', '0')
                    ->first();
        if(!($reqLeft->total != null || $reqLeft->total > 0)){
            $ch->setQueueing($code, "false");
        }

        $failed = []; $success = []; $services = [];
        foreach ($data as $key => $value) {

            if(!isset($services[$value['code']])){
                $serviceClassName = "App\\Http\\Services\\" . ucfirst(strtolower($value['code']));
                if (!class_exists($serviceClassName)){
                    $this->logger->error($logPrefix."Executing ID : ".$value['q_report_id']." | Service Unknown : ".$serviceClassName);
                    $this->updateData($value['id'], $value['q_report_id'], "6");
                    continue;
                }
                if (!method_exists($serviceClassName, "validateResponse")){
                    $this->logger->error($logPrefix."Executing ID : ".$value['q_report_id']." | Method Unknown : ".$serviceClassName." | validateResponse");
                    $this->updateData($value['id'], $value['q_report_id'], "6");
                    continue;
                }
                $services[$value['code']] = new $serviceClassName();
            }

            if($value['body_type'] != "json"){
                $value['body_request'] = json_decode($value['body_request'], true);
            }

            $value['header'] = json_decode($value['header'], true);

            $result = $ch->curl($value['url'], $value['body_request'], $value['header'], $value['http_type']);
            $this->logger->info($logPrefix."Response Curl : ".json_encode($result));

            if($services[$value['code']]->validateResponse($result)){
                $this->logger->info($logPrefix."Request Success : ".json_encode($result));
                $this->updateData($value['id'], $value['q_report_id'], "7");
            }else{
                $this->logger->error($logPrefix."Request Failed : ".json_encode($result));
                $this->updateData($value['id'], $value['q_report_id'], "6");
            }
        }
        exit("Job Executed");
    }

    private function updateData($id, $idR, $status){
        $logPrefix = __FUNCTION__."\t";
        $this->logger->info($logPrefix."Updating Data Queueing, id :".$id." and Queueing Report, id : ".$idR." with status : ".$status);
        if($status == '7'){
            $updateId = Queueing::where('id', $id)
                        ->delete();
        }else{
            $updateId = Queueing::where('id', $id)
                        ->update(['status' => $status]);
        }
        $updateIdR = QueueingReport::where('id', $idR)
                    ->update(['status' => $status]);
        $this->logger->info($logPrefix."Result : ".$updateId." & ".$updateIdR);
    }
}
