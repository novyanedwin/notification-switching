<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Helpers\CommonHelper;
use App\Models\Queueing;
use App\Models\QueueingReport;
use Logger;

use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;


if (!defined("SESSIONID")) define("SESSIONID", uniqid());

class SenderEmail extends Command
{
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "worker:email {thread_number=null} {priority=null} {limit=100}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "worker:email {thread_number} {priority} {limit=100}";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    protected $code = "email";
    protected $batchName = "";
    protected $logger;

    public function handle()
    {
        $this->logger = new Logger("Job_Sender");

        $priority   = $this->argument("priority");
        $tNumber    = $this->argument("thread_number");
        $limit      = $this->argument("limit");

        $code = $this->code;

        if($tNumber != "null"){
            if($tNumber % 2 == 0){
                $type = "even";
            }else{
                $type = "odd";
            }
        }else{
            $type = "all";
        }

        $this->batchName = "sender_".$code."_".$type;
        
        $ch =  new CommonHelper;
        
        if($ch->checkRunning($this->batchName)){
            $this->logger->error("Job Is Still Running : ".$this->batchName);
            exit("Still Running");
        }

        if(!$ch->doQueueing($code)){
            $this->logger->info("Nothing Executed : ".$this->batchName);
            exit("Nothing Executed");
        }

        if((int) $limit > 100){
            $this->logger->error("Forcing Limit to 100, Limit Requested : ".$limit);
            $limit = 100;
        }

        $ch->batchRunning($this->batchName, "true");
        $data = Queueing::where('code', $code)
                ->where('status', '0')
                ->where(function($query) use($type, $priority){
                    if($type == "odd"){
                        $query->whereRaw("(id % 2) != 0");
                    }else if ($type == "even"){
                        $query->whereRaw("(id % 2) = 0");
                    }

                    if($priority != "null"){
                        $query->where('priority', $priority);
                    }
                })
                ->limit($limit)
                ->orderBy(($type != "all" ? 'id' : 'priority'), ($type != "all" ? 'asc' : 'desc'))
                ->get()
                ->toArray();
        
        if(count($data) < 1){
            $ch->setQueueing($code, "false");
            $ch->batchRunning($this->batchName, "false");
            exit("Empty Data");
        }
        $idList = array_column($data, "id");
        $item = count($idList);
        
        $updateId = Queueing::whereIn('id', $idList)
                    ->update(['status' => "1"]);
        
        if($item != $updateId){
            $this->logger->error("Unmatch Update : ".$updateId." != ".$item);
        }
        $ch->batchRunning($this->batchName, "false");

        $reqLeft = Queueing::selectRaw("count(id) as total")
                    ->where('code', $code)
                    ->where('status', '0')
                    ->first();
        if(!($reqLeft->total != null || $reqLeft->total > 0)){
            $ch->setQueueing($code, "false");
        }

        $failed = []; $success = []; $services = [];

        $name     = "Lock_Email";
        $path     = storage_path("/app/");
        $filename = $path.$name;
        
        foreach ($data as $key => $value) {

            $value['body_request'] = json_decode($value['body_request'], true);
            $value['header'] = json_decode($value['header'], true);
            
            $lock = true;
            while($lock){
                $fp = fopen($filename, 'w+') or die('have no access to '.$filename);
                if(flock($fp, LOCK_EX)){
                    $lock = false;
                }
                $this->mailSender($value, $key, $tNumber);
                usleep(500000); //sleep 500ms after success processing email;
                flock($fp, LOCK_UN);
                fclose($fp);
                
            }
            
        }
        exit("Job Executed");
    }

    private function updateData($id, $idR, $status){
        $this->logger->info("Updating Data Queueing, id :".$id." and Queueing Report, id : ".$idR." with status : ".$status);
        if($status == '7'){
            $updateId = Queueing::where('id', $id)
                        ->delete();
        }else{
            $updateId = Queueing::where('id', $id)
                        ->update(['status' => $status]);
        }
        $updateIdR = QueueingReport::where('id', $idR)
                    ->update(['status' => $status]);
        $this->logger->info("Result : ".$updateId." & ".$updateIdR);
    }

    private function mailSender($data, $key, $threadNumber){

        $host           = $data['header']['host'];
        $port           = $data['header']['port'];
        $smtpSecure     = $data['header']['SMTPSecure'];
        $username       = $data['header']['username'];
        $password       = $data['header']['password'];
        $from           = $data['header']['username'];
        $fromName       = $data['header']['fromName'];

        $to             = $data['body_request']['to'];
        $body           = @base64_decode($data['body_request']['data']['content']);
        $body_text      = @base64_decode($data['body_request']['data']['content_text']);
        $subject        = $data['body_request']['data']['subject'];

        if(isset($data['header']['bccTo'])){
            if(is_array($data['header']['bccTo'])){
                $bcc = $data['header']['bccTo'];
            }else{
                $bcc = [$data['header']['bccTo']];
            }
        }else{
            $bcc = [];
        }
        
        /* Testing Purpose
            unset($data['body_request']['data']['content']);
            unset($data['body_request']['data']['content_text']);
            $data['body_request']['id'] = $data['id'];
        */
        
        $this->logger->info("Sending data : ".$data['id']);

        try {
            $transport = (new Swift_SmtpTransport(
                            $host,
                            $port, 
                            $smtpSecure))
                        ->setUsername($username)
                        ->setPassword($password);
            $mailer = new Swift_Mailer($transport);
            $message = (new Swift_Message($subject))
                        ->setFrom([$from => $fromName])
                        ->setReplyTo([$from => $fromName])
                        ->setTo([$to])
                        ->setBcc($bcc)
                        ->setBody($body_text)
                        ->addPart($body, "text/html");
            $result = $mailer->send($message);
            
            $this->logger->info("Result data : ".$result);
            if($result){
                $this->updateData($data['id'], $data['q_report_id'], "7");
            }else{
                $this->updateData($data['id'], $data['q_report_id'], "6");
            }
        } catch (\Throwable $th) {
            $this->logger->error("Result data : ".$th->getMessage());
            $this->updateData($data['id'], $data['q_report_id'], "6");
        }
    }
}
