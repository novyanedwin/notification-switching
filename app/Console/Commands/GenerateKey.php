<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Helpers\CommonHelper;

class GenerateKey extends Command
{
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "generate:key {key}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generating Key";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $key = $this->argument("key");
        echo CommonHelper::enc($key);
    }
}
