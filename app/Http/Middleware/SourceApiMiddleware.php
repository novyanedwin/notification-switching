<?php

namespace App\Http\Middleware;

use App\Helpers\CommonHelper;
use Closure;
use Exception;
use App\Models\SourceApi;
use Illuminate\Http\Request;
use Logger;

class SourceApiMiddleware
{
    protected $logger;
    
    public function handle(Request $request, Closure $next, $guard = null)
    {
        $key = $request->header("API-KEY");

        if (!$key) return ["return_status" => false, "errorMessage" => "Key harus terisi"];

        $ch = new CommonHelper;
        try {
            $credentials = $ch->dec($key);
            if($credentials === false) return ["return_status" => false, "errorMessage" => "Gagal Mendapatkan API Key"];
        } catch (Exception $e) {
            $this->logger = new Logger;
            $this->logger->error(__CLASS__." Error : ".$e->getMessage());
            return ["return_status" => false, "errorMessage" => "Gagal Mendapatkan API Key #2"];
        }

        $check = SourceApi::where("key", $credentials)->first();

        if (empty($check)) return ["return_status" => false, "errorMessage" => "API Key Tidak Terdaftar"];

        Config()->set('API_ID', $check->id);

        return $next($request);
    }
}
