<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Logger;

class BuildApiResponse
{
    protected $logger;

    public static function transformResponse($resOri)
    {

		if(!isset($resOri['status'])){
			$resOri['status'] = "error";
		}

		if(!isset($resOri['code'])){
			if($resOri['status'] == "success"){
				$resOri['code'] = 200;
			}else{
				$resOri['code'] = 400;
			}
		}

		if((!isset($resOri['message']) && !isset($resOri['data'])) && $resOri['status'] == "error"){
			$resOri['message'] = "Internal System Error!";
		}else if(!isset($resOri['message'])){
			$resOri['message'] = "";
		}

		if(!isset($resOri['data'])){
			$resOri['data'] = null;
		}

		$res = [
            "status" => $resOri['status'],
            "data" => $resOri['data'],
            "message" => $resOri['message'],
        ];

		$code = $resOri['code'];

        return new Response($res, $code);
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $this->logger = new Logger;
        $response = $next($request);
        try {
            $original = $response->getOriginalContent();
        } catch (\Throwable $th) {
            $original = $response;
            if (!is_array($original)) {
                $original = [
                    'status' => "error",
                    'message' => "Something Bad Happen!",
                ];
            }
        }

		if (!is_array($original)) {
			$original = [
				'status' => "error",
				'message' => "Something Bad Happen!",
			];
		}
		$response = $this->transformResponse($original);

        $this->logger->info("Request\t\t: " . $request->ip() . " - [" . $request->method() . "] " . $request->getPathInfo() . " Header :" . json_encode($request->header()) . " | Data : " . json_encode($request->all()));
        $this->logger->info("Response\t: " . json_encode($response));

        return $response;
    }
}
