<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\CommonHelper;
use Illuminate\Support\Str;
use App\Constants\MappErrorCode;
use Illuminate\Support\Facades\Validator;

class NotificationRequestController extends Controller
{
    protected $serviceVendor;
    protected $vendorName;

    private function validator($request){
        $data = [
            'rules' => [
                'receiver'      => "required",
                'message'       => "required",
                'vendor'        => "nullable"
            ],
            'message' => [
                'required' => 'The :attribute field is required!.',
                'in' => 'Allowed :attribute field: :values'
            ]
        ];

        return Validator::make($request->all(),  $data['rules'],  $data['message']);
    }

    private function selectVendor($vendor = null){
        $strListActive = env("ACTIVE_VENDOR");
        $strListDisable = env("DISABLE_VENDOR");
        $tmpActiveVendor = array_filter(explode(",", $strListActive));
        $disableVendor = array_filter(explode(",", $strListDisable));
        $activeVendor = array_values(array_diff($tmpActiveVendor, $disableVendor));
        if(!empty($activeVendor)){
            if($vendor != null){
                if($this->in_arrayi($vendor, $activeVendor))
                    return $this->initService($vendor);
                else{
                    return MappErrorCode::VENDOR_NOT_AVAILABLE;
                }
            }else{
                $rIndex = 0;
                if(count($activeVendor) > 1){
                    $rIndex = rand(0, (count($activeVendor)-1));
                }
                $vendor = $activeVendor[$rIndex];
                return $this->initService($vendor);
            }
        }else{
            return MappErrorCode::VENDOR_NOT_IMPLEMENTED;
        }
    }

    private function initService($code){
        $serviceClassName = "App\\Http\\Services\\" . ucfirst(strtolower($code));
            if (!class_exists($serviceClassName)) return MappErrorCode::VENDOR_NOT_IMPLEMENTED;

            $notificationService = new $serviceClassName();
            if (!method_exists($notificationService, "send")) return MappErrorCode::VENDOR_NOT_IMPLEMENTED;
            else {
                $this->serviceVendor = $notificationService;
                $this->vendorName = $code;
                return true;
            }
    }

    public function submit(Request $request){

        $validator = $this->validator($request);
        if ($validator->fails()) {
            $return['return_status'] = 'error';
            $return['errorMessage'] = "Bad Request!";
            $return['errorDetail'] = $validator->errors();
            return $return;
        }

        $initiateVendor = $this->selectVendor($request->input('vendor'));
        if($initiateVendor !== true){
            return $initiateVendor;
        }

        $res = $this->serviceVendor->send($request);
        if($res['status'] == "error" && env("BACKUP", null) != null){
            $oldVendor = $this->vendorName;
            $initiateVendor = $this->initService(env("BACKUP"));
            if($initiateVendor !== true){
                return $initiateVendor;
            }

            $res = $this->serviceVendor->send($request);
            if($res['status'] == "success"){
                $res['data']['retry_form'] = $oldVendor;
            }
        }
        return $res;
        
    }
}
