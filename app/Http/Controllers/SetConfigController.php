<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\CommonHelper;
use Illuminate\Support\Str;
use App\Constants\MappErrorCode;
use Illuminate\Support\Facades\Validator;

class SetConfigController extends Controller
{

    private function validator($request){

        $data = [
            'rules' => [
                'process' => "required|in:disable,active,backup",
                'vendor_name' => "required|string",
            ],
            'message' => [
                'required' => 'The :attribute field is required!.',
                'in' => 'Allowed :attribute field: :values'
            ]
        ];

        return Validator::make($request->all(),  $data['rules'],  $data['message']);
    }

    public function setEnvironmentValue($key, $value)
    {

        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);

        $str .= "\n"; // In case the searched variable is in the last line without \n
        $keyPosition = strpos($str, "{$key}=");
        $endOfLinePosition = strpos($str, "\n", $keyPosition);
        $oldLine = substr($str, $keyPosition, $endOfLinePosition - $keyPosition);

        // If key does not exist, add it
        if (!$keyPosition || !$endOfLinePosition || !$oldLine) {
            $str .= "{$key}={$value}\n";
        } else {
            $str = str_replace($oldLine, "{$key}={$value}", $str);
        }

        $str = substr($str, 0, -1);
        if (!file_put_contents($envFile, $str)) return false;
        return true;

    }

    private function setBackup(){
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);
        file_put_contents($envFile.".backup", $str);
    }

    private function rollbackEnv(){
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile.".backup");
        file_put_contents($envFile, $str);
    }

    private function removeBackup(){
        $envFile = app()->environmentFilePath();
        unlink($envFile.".backup");
    }

    public function setupConfig(Request $request){

        $validator = $this->validator($request);
        if ($validator->fails()) {
            $return['return_status'] = 'error';
            $return['errorMessage'] = "Bad Request!";
            $return['errorDetail'] = $validator->errors();
            return $return;
        }

        switch ($request->input('process')) {
            case 'active':
                return $this->updateActive($request->input("vendor_name"));
            case 'disable':
                return $this->updateDisable($request->input("vendor_name"));
            case 'backup':
                return $this->updateBackup($request->input("vendor_name"));
        }
    }

    private function updateActive($data){
        $active = array_filter(explode(",",env("ACTIVE_VENDOR")));
        $disable = array_filter(explode(",",env("DISABLE_VENDOR")));
        if(!$this->in_arrayi($data, $active)){
            $serviceClassName = "App\\Http\\Services\\" . ucfirst(strtolower($data));
            if (!class_exists($serviceClassName)) return MappErrorCode::VENDOR_NOT_IMPLEMENTED;
            array_push($active, $data);
        }

        if($this->in_arrayi($data, $disable)){
            $index = array_search(strtolower($data), array_map('strtolower', $disable));
            unset($disable[$index]);
            if(!$this->setEnvironmentValue("DISABLE_VENDOR", implode(",", $disable))){
                $this->rollbackEnv();
                return ['status' => 'error', 'data' => "Failed Removing {$data} from DISABLE_VENDOR"];
            }
        }

        if($this->setEnvironmentValue("ACTIVE_VENDOR", implode(",", array_map('strtoupper', $active)))){
            return ['status' => 'success', 'data' => "Successfully Activate {$data}"];
        }else{
            return ['status' => 'error', 'data' => "Failed Adding {$data} to ACTIVE_VENDOR"];
        }
    }

    private function updateDisable($data){
        $active = array_filter(explode(",",env("ACTIVE_VENDOR")));
        $disable = array_filter(explode(",",env("DISABLE_VENDOR")));

        if(!$this->in_arrayi($data, $disable)){
            array_push($disable, $data);
        }

        if($this->in_arrayi($data, $active)){
            if(count($active) < 2){
                return ['status' => 'error', 'data' => "Vendor Active Cant be Empty"];
            }
            $index = array_search(strtolower($data), array_map('strtolower', $active));
            unset($active[$index]);
            if(!$this->setEnvironmentValue("ACTIVE_VENDOR", implode(",", $active))){
                $this->rollbackEnv();
                return ['status' => 'error', 'data' => "Failed Removing {$data} from ACTIVE_VENDOR"];
            }
        }

        if($this->setEnvironmentValue("DISABLE_VENDOR", implode(",", array_map('strtoupper', $disable)))){
            return ['status' => 'success', 'data' => "Successfully Disable {$data}"];
        }else{
            return ['status' => 'error', 'data' => "Failed Adding {$data} to DISABLE_VENDOR"];
        }
    }

    private function updateBackup($data){

        $serviceClassName = "App\\Http\\Services\\" . ucfirst(strtolower($data));
        if (!class_exists($serviceClassName)) return MappErrorCode::VENDOR_NOT_IMPLEMENTED;

        if($this->setEnvironmentValue("BACKUP", strtoupper($data))){
            return ['status' => 'success', 'data' => "Successfully Set Backup to {$data}"];
        }else{
            return ['status' => 'error', 'data' => "Failed Set {$data} as Backup"];
        }
    }

}
