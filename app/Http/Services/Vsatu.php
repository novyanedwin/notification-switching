<?php

namespace App\Http\Services;

use App\Helpers\CommonHelper;

class Vsatu{

    protected $config;
    protected $commonHelper;
    const STATUS_SUCCESS = "0";
    const STATUS_FAILED = "1";
    const SEND_HTTP_TYPE = "POST";

    public function __construct(){
        $config = [
            'client_key'    => env("VSATU_KEY"),
            'secret_key'    => env("VSATU_SECRET"),
            'host'          => env("VSATU_HOST"),
        ];
        $this->config = $config;
        $this->commonHelper = new CommonHelper;
    }

    public function send($request){
        $param = $this->buildParam($request);
        $header = $this->buildHeader();
        $response = $this->commonHelper->curl($this->config['host'], json_encode($param), $header, self::SEND_HTTP_TYPE);
        if($this->validateResponse($response)){
            return ['status' => 'success', 'data' => $response];
        }else{
            return ['status' => 'error', 'data' => 'Notification Failed to Sent!'];
        }
    }

    private function buildParam($request){
        return [
            'channel' => [
                'sms' => [
                    "message" => $request->input("message"),
                    "msisdn" => $request->input("msisdn"),
                    "backup_on" => "",
                    "backup_exp" => ""
                ]
            ],
            'signature' => $this->buildSign(),
        ];
    }

    private function buildSign(){
        return hash("sha256", $this->config['client_key'] . $this->config['secret_key'] . time());
    }

    private function buildHeader(){
        return [
            'Content-Type:application/json'
        ];
    }

    public function validateResponse($data){
        if(isset($data['rc'])){
            if($data['rc'] == self::STATUS_SUCCESS){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    
}

?>