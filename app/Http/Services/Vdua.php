<?php

namespace App\Http\Services;

use App\Helpers\CommonHelper;

class Vdua{

    protected $config;
    protected $commonHelper;
    const STATUS_SUCCESS = "00";
    const STATUS_FAILED = "06";
    const SEND_HTTP_TYPE = "POST";

    public function __construct(){
        $config = [
            'client_key'    => env("VDUA_KEY"),
            'secret_key'    => env("VDUA_SECRET"),
            'host'          => env("VDUA_HOST"),
        ];
        $this->config = $config;
        $this->commonHelper = new CommonHelper;
    }

    public function send($request){
        $param = $this->buildParam($request);
        $header = $this->buildHeader();
        $response = $this->commonHelper->curl($this->config['host'], json_encode($param), $header, self::SEND_HTTP_TYPE);
        if($this->validateResponse($response)){
            return ['status' => 'success', 'data' => $response];
        }else{
            return ['status' => 'error', 'data' => 'Notification Failed to Sent!'];
        }
    }

    private function buildParam($request){
        return [
            'sms' => [
                "message" => $request->input("message"),
                "msisdn" => $request->input("msisdn")
            ]
        ];
    }

    private function buildSign(){
        return base64_encode($this->config['client_key'].":".$this->config['secret_key']);
    }

    private function buildHeader(){
        return [
            'Content-Type:application/json',
            'Authorization:Basic '.$this->buildSign()
        ];
    }

    public function validateResponse($data){
        if(isset($data['responseCode'])){
            if($data['responseCode'] == self::STATUS_SUCCESS){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    
}

?>