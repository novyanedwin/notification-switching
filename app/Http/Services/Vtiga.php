<?php

namespace App\Http\Services;

use App\Helpers\CommonHelper;

class Vtiga{

    protected $config;
    protected $commonHelper;
    const STATUS_SUCCESS = "success";
    const STATUS_FAILED = "error";
    const SEND_HTTP_TYPE = "GET";

    public function __construct(){
        $config = [
            'client_key'    => env("VTIGA_KEY"),
            'secret_key'    => env("VTIGA_SECRET"),
            'host'          => env("VTIGA_HOST"),
        ];
        $this->config = $config;
        $this->commonHelper = new CommonHelper;
    }

    public function send($request){
        $param = $this->buildParam($request);
        $header = $this->buildHeader();
        $response = $this->commonHelper->curl($this->config['host']."?".http_build_query($param), [], $header, self::SEND_HTTP_TYPE);
        if($this->validateResponse($response)){
            return ['status' => 'success', 'data' => $response];
        }else{
            return ['status' => 'error', 'data' => 'Notification Failed to Sent!'];
        }
    }

    private function buildParam($request){
        return [
            'sms' => [
                "message" => $request->input("message"),
                "msisdn" => $request->input("msisdn")
            ]
        ];
    }

    private function buildSign(){
        return base64_encode($this->config['client_key'].":".$this->config['secret_key']);
    }

    private function buildHeader(){
        return [
            'Content-Type:application/json',
            'Authorization:Basic '.$this->buildSign()
        ];
    }

    public function validateResponse($data){
        if(isset($data['status'])){
            if($data['status'] == self::STATUS_SUCCESS){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    
}

?>