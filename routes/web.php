<?php

$router->get("/", function () use ($router) {
    return "Welcome to Notification Service";
});

$router->post("/send", "NotificationRequestController@submit");
$router->post("/set", "SetConfigController@setupConfig");
